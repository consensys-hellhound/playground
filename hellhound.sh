#!/bin/sh
docker-compose -f infra/elastic.yml -f infra/jaeger.yml -f infra/cassandra.yml up --build -d
sleep 30s
docker-compose -f pythia.yml -f iris.yml up --build -d